let username;
let password;
let role;

// Login

function loginEnterDetails() {
    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = prompt("Enter your role:");

    if (username === "" || password === "" || role === "") {
        alert("You're input should not be empty");
    }
    else {
        switch(role) {
            case "admin":
                alert("Welcome back to the class portal, admin");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range");
                break;
        }
    }
}

loginEnterDetails();

// Check Average

function calculateAverage(aveOne, aveTwo, aveThree, aveFour) {
    let average = (aveOne + aveTwo + aveThree + aveFour) / 4; 
    average = Math.round(average);
    //console.log(average)

    if (average <= 74) {
        console.log("Hello, student, your average is " + average + " The letter equivalent is F");
    } 
    else if (average >= 75 && average <= 79) {
        console.log("Hello, student, your average is " + average + " The letter equivalent is D");
    }
    else if (average >= 80 && average <= 84) {
        console.log("Hello, student, your average is " + average + " The letter equivalent is C");
    }
    else if (average >= 85 && average <= 89) {
        console.log("Hello, student, your average is " + average + " The letter equivalent is B");
    }
    else if (average >= 90 && average <= 95) {
        console.log("Hello, student, your average is " + average + " The letter equivalent is A");
    }
    else if (average > 96) {
        console.log("Hello, student, your average is " + average + " The letter equivalent is A+");
    }
}

// calculateAverage(70, 70, 72, 71);
// calculateAverage(76, 76, 77, 79);
// calculateAverage(81, 83, 84, 85);
// calculateAverage(87, 88, 88, 89);
// calculateAverage(91, 90, 92, 90);
// calculateAverage(96, 95, 97, 97);