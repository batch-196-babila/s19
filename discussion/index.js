let num1 = 0;

if (num1 === 0) {
    console.log("The value of num1 is 0");
}

num1 = 25;

if (num1 === 0) {
    console.log("The current value of num1 is still 0");
}

let city = "New York";

if (city === "New Jersey") {
    console.log("Welcome to New Jersey");
}

if (city === "New Jersey") {
    console.log("Welcome to New Jersey");
} else {
    console.log("This is not New Jersey");
}

if (num1 < 20) {
    console.log("num1's value is less than 20");
} else {
    console.log("num1's value is more than 20")
}

function cityChecker(city) {
    if (city === "New York") {
        console.log("Welcome to the Empire State!");
    } else {
        console.log("You're not in New York");
    }
}

cityChecker("New York");
cityChecker("Manila");

function checkBudget(budget) {
    if (budget <= 40000) {
        console.log("You're still within budget");
    } else {
        console.log("You are currently over budget");
    }
    return budget;
}

checkBudget(40000);
checkBudget(38000);

// Else If statement

let city2 = "Manila";

if (city2 === "New York") {
    console.log("Welcome to New York")
} 
else if (city2 === "Manila") {
    console.log("Welcome to Manila");
} 
else {
    console.log("I don't know where you are");
}

// let role = "admin";

// if (role === "developer") {
//     console.log("Welcome back, developer");
// } 

// else if (role === "") {
//     console.log("Role provided is invalid");
// }

// else {
//     console.log("Role is out bounds");
// }

// Multiple Else If Statements

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return "Not a typhoon yet";
    }
    else if (windSpeed <= 61) {
        return "Tropical Depression Detected";
    }
    else if (windSpeed >= 62 && windSpeed <= 88) {
        return "Tropical Storm Detected";
    }
    else if (windSpeed >= 89 && windSpeed <= 117) {
        return "Severe Tropical Storm Detected";
    }
    else {
        return "Typhoon Detected";
    }
}

let typhoonMessage1 = determineTyphoonIntensity(29);
let typhoonMessage2 = determineTyphoonIntensity(62);
let typhoonMessage3 = determineTyphoonIntensity(61);
let typhoonMessage4 = determineTyphoonIntensity(88);
let typhoonMessage5 = determineTyphoonIntensity(117);
let typhoonMessage6 = determineTyphoonIntensity(120);

console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);

// Truthy and Falsy Values
// Sample of truthy
// 1. True

if (1) {
    console.log("1 is truthy");
}

if ([]) {
    console.log("[] empty array is truthy");
}

// 2. False

if (0) {
    console.log("0 is falsy"); // no print means false
}

if (undefined) {
    console.log("undefined is not falsy"); 
} else {
    console.log("Undefined is falsy");
}

// TERNARY - shortened if else statements
// implicitly return value - meaning no need for return value

// syntax condition ? ifTrue : ifFalse;

let age = 17;
let result = age < 18 ? "Underaged." : "Legal Age";
console.log(result);

// let result2 = if (age < 18) {
//     return "Underage";
// } else {
//     return "Legal Age";
// }

// console.log(result2); error

// Switch Statement
// Evaluate and expression and match the expression to a case clause
// An expression will be compared against diff cases
// used as an alternarive from if-else statement
// .toLowerCase(); = string only - built-in method

let day = prompt("What day of the week is it today").toLowerCase();

console.log(day);

switch(day) {
    case "monday": 
        console.log("The color of the day is red");
        break;
    case "tuesday": 
        console.log("The color of the day is orange");
        break;
    case "wednesday": 
        console.log("The color of the day is yellow");
        break;
    case "thursday": 
        console.log("The color of the day is gray");
        break;
    case "friday": 
        console.log("The color of the day is green");
    break;
        case "saturday": 
        console.log("The color of the day is purple");
        break;
    case "sunday": 
        console.log("The color of the day is brown");
        break;
    default:
        console.log("Please enter a valid day");
        break;
}

// Try-Catch-Finally

try {
    alert(determineTyphoonIntensity(50));
} catch (error) { // error err or e
    //console.log(typeof error);

    //error.message allows to access info about the error
    console.log(error.message);

    // Error handling
} finally {
    // finally will run regardless of success or failure of the try statement
    alert("Intensity updates will show in a new alert");
}

console.log("Will we continue to the next line code? : Yes");